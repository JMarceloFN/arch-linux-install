# ARCH LINUX INSTALL

#### Adjust keyboard layout, if needed

loadkeys br-abnt2

#### Test Internet connection

ping archlinux.org

#### Disk partitioning

##### UEFI INSTALLATION

cfdisk /dev/device (it deppends which device you’re going to use) 

/dev/nvme0n1p1 (512 M for /boot/efi) – Set this partition as EFI  
/dev/nvme0n1p2 (90G for / )  
/dev/nvme0n1p3 (2G for /tmp)  
/dev/nvme0n1p4 (12G for /var)  
/dev/nvme0n1p5 (32G for SWAP)  
/dev/nvme0n1p6 (Left space  for /mnt/games)  
/dev/sdb1 (for /home - do not format)  
/dev/sda1 (for /mnt/torrents - do not format)  

mkfs.fat -F32 /dev/nvme0n1p1  
mkfs.ext4 /dev/nvme0n1p2  
mkfs.ext4 /dev/nvme0n1p3  
mkfs.ext4 /dev/nvme0n1p4  
mkswap /dev/nvme0n1p5  
mkfs.ext4 /dev/nvme0n1p6  


#### Mounting points  

mount /dev/nvme0n1p2 /mnt  
mkdir -p /mnt/boot/efi  
mkdir /mnt/home  
mkdir -p /mnt/mnt/games  
mkdir -p /mnt/mnt/torrents  
mkdir -p /mnt/var    
mkdir -p /mnt/tmp  
mount /dev/sdb1 /mnt/home  
mount /dev/nvme0n1p1 /mnt/boot/efi  
mount /dev/nvme0n1p3 /mnt/tmp  
mount /dev/nvme0n1p4 /mnt/var  
swapon /dev/nvme0n1p5  
mount /dev/nvme0n1p6 /mnt/mnt/games  
mount /dev/sda1 /mnt/mnt/torrents  

##### BIOS INSTALLATION

cfdisk /dev/sdx (it deppends which device you’re going to use)  

/dev/sda1 (50 Gb for / )  
/dev/sdb1 (All the rest to /home )  

note: these are my devices layout, i’ll make a swap file later  

mkfs.ext4 /dev/sda1  
mkfs.ext4 /dev/sdb1  

#### Mounting points

mount /dev/sda1 /mnt  
mkdir /mnt/home  
mount /dev/sdb1 /mnt/home  
mkdir -p /mnt/mnt/games  
mkdir -p /mnt/mnt/torrents   
mount /dev/sda3 /mnt/mnt/games  
mount /dev/sdc1 /mnt/mnt/torrents  


#### Choose closest mirrors

reflector -c US --age 12 --protocol https --sort rate --verbose --save /etc/pacman.d/mirrorlist

#### Base packages install

pacstrap /mnt base base-devel linux linux-firmware linux-lts linux-lts-headers vim alacritty

#### Generate Fstab

genfstab -U -p /mnt >> /mnt/etc/fstab  

(To be sure the entries are correct type cat /mnt/etc/fstab)  

#### Entering the system

arch-chroot /mnt

#### Adjusting timezone

ln -sf /usr/share/zoneinfo/region/city /etc/localtime

#### Adjusting system clock

hwclock --systohc

#### Configuring system language (mine is portuguese from Brazil)

vim /etc/locale.gen  

Uncomment the line pt_BR.UTF-8 UTF-8 save and exit the file  

#### Generate the localization file

locale-gen

#### Configuring language variable

echo "LANG=pt_BR.UTF-8" >> /etc/locale.conf  
echo "KEYMAP=br-abnt2" >> /etc/vconsole.conf  

#### Configuring Hostname

vim /etc/hostname  

Inside put the hostname of your preference  

#### Configuring hosts file

vim /etc/hosts  

Inside replicate as shown below  

127.0.0.1 localhost.localdomain localhost  
::1 localhost.localdomain localhost  
127.0.1.1 yourhostname.localdomain yourhostname  

Save and exit the file

#### Root password

passwd

#### Adding an user

useradd -m -g users -G wheel,power,storage,games,adm,systemd-journal,audio,optical,video,lp username

#### Setting new user password

passwd username

#### Installing some useful packages

pacman -S dosfstools os-prober mtools network-manager-applet networkmanager wpa_supplicant wireless_tools dialog xterm

#### Adding user non root for sudo privileges

visudo /etc/sudoers  

Uncomment the line below  

%wheel ALL=(ALL) ALL  

Save and exit  

#### Grub installation

##### EFI

pacman -S grub-efi-x86_64 efibootmgr  

grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB –recheck  

cp /usr/share/locale/en@quot/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo  

##### BIOS

pacman -S grub  

grub-install --target=i386-pc --recheck /dev/sda  

cp /usr/share/locale/en@quot/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo  

Grub application  

grub-mkconfig -o /boot/grub/grub.cfg  

#### Ending the base install

Type exit, to leave arch-chroot  

Unmout the partions  

umount -R /mnt  

Then reboot  

#### Starting network connection

systemctl status NetworkManager  
systemctl start NetworkManager  

#### Set console layout as PT-BR

As root, type:

localectl set-x11-keymap br abnt2

Logout and logon

#### Pacman configuration

Edit pacman.conf  

sudo vim /etc/pacman.conf

Uncomment  
#Color  
Uncomment enable Parallel Downloads  
#ParallelDownloads = 5  
Add in Misc options   
ILoveCandy  
Uncomment  
[multilib]  
Include = /etc/pacman.d/mirrorlist  

#### Installing more basic packages

pacman -Syu  
pacman -S xorg-server xorg-xinit  
pacman -S --needed lib32-mesa vulkan-radeon lib32-vulkan-radeon vulkan-icd-loader lib32-vulkan-icd-loader

#### INSTALLING DESKTOP ENVIRONMENTS AND WINDOW MANAGERS

##### I3WM

sudo pacman -S i3-wm i3blocks i3status lxappearance rofi dmenu polybar picom dunst nitrogen pcmanfm jq   
From AUR i3lock-fancy-git diodon-git ulauncher  

##### CINNAMON

sudo pacman -S cinnamon cinnamon-translations

#### Installing Lightdm Display Manager

sudo pacman -S lightdm lightdm-gtk-greeter lightdm-webkit2-greeter  

Edit the file /etc/lightdm/lightdm.conf  

sudo vim /etc/lightdm/lightdm.conf  

greeter-session=lightdm-webkit2-greeter <======Uncomment and type as the same on the left  

To enable numlock at startup, install the package numlockx then add the lines below

[Seat:*]
greeter-setup-script=/usr/bin/numlockx on

Initializing and enabling  

sudo systemctl start lightdm.service  
sudo systemctl enable lightdm.service  

reboot  

#### Packages selection

#### System utils  

sudo pacman -S vifm gnome-font-viewer man-db ntfs-3g arch-audit cups cups-pdf gparted p7zip unrar xdg-user-dirs rsync mlocate jre-openjdk ufw gnome-disk-utility system-config-printer npm python-pip python-qtpy traceroute hdparm wget ntp git util-linux alacritty btop ncdu galculator numlockx pcmanfm mesa-utils gnome-system-monitor gprename reflector remmina openvpn intel-ucode coreutils sysstat htop neofetch engrampa gvfs gnome-keyring seahorse qt5ct dmidecode networkmanager-openvpn freerdp nemo cinnamon-translations nemo-fileroller nemo-preview nemo-share smartmontools gsmartcontrol jq lesspipe pacman-contrib lshw pwgen fortune-mod timeshift

#### Internet  

sudo pacman -S firefox firefox-i18n-pt-br transmission-gtk  

#### Office suite  

sudo pacman -S libreoffice-fresh libreoffice-fresh-pt-br foliate xreader xed  

#### Graphics  

sudo pacman -S gimp inkscape

#### Bluetooth 

sudo pacman -S bluez bluez-utils blueman 

sudo systemctl enable bluetooh.service 

sudo systemctl start bluetooth.service

#### Media  

sudo pacman -S celluloid soundconverter mpv subtitleeditor kid3 lollypop easytag picard vlc smplayer ffmpegthumbnailer aegisub yt-dlp webp-pixbuf-loader gstreamer gst-plugins-bad gst-plugins-base  gst-plugins-base-libs gst-plugins-good gst-plugins-ugly xine-lib libdvdcss libdvdread lame   

#### DVD tools  

sudo pacman -S dvd+rw-tools cdrtools cdrdao xfburn  

#### Themes  

sudo pacman -S adapta-gtk-theme arc-gtk-theme arc-icon-theme papirus-icon-theme archlinux-wallpaper adobe-source-sans-pro-fonts arc-solid-gtk-theme breeze-icons oxygen-icons powerline-fonts  

#### Fonts  

sudo pacman -S ttf-anonymous-pro ttf-bitstream-vera ttf-croscore ttf-dejavu ttf-droid ttf-ibm-plex ttf-liberation ttf-roboto-mono noto-fonts gnu-free-fonts ttf-liberation ttf-linux-libertine ttf-ubuntu-font-family terminus-font   

#### Audio  

sudo pacman -S pipewire pavucontrol

#### Games  

sudo pacman -S --needed steam wine wine-staging winetricks giflib lib32-giflib libpng lib32-libpng libldap lib32-libldap gnutls lib32-gnutls mpg123 lib32-mpg123 openal lib32-openal v4l-utils lib32-v4l-utils libpulse lib32-libpulse libgpg-error lib32-libgpg-error alsa-plugins lib32-alsa-plugins alsa-lib lib32-alsa-lib libjpeg-turbo lib32-libjpeg-turbo sqlite lib32-sqlite libxcomposite lib32-libxcomposite libxinerama lib32-libgcrypt libgcrypt lib32-libxinerama ncurses lib32-ncurses opencl-icd-loader lib32-opencl-icd-loader libxslt lib32-libxslt libva lib32-libva gtk3 lib32-gtk3 gst-plugins-base-libs lib32-gst-plugins-base-libs vulkan-icd-loader lib32-vulkan-icd-loader  

##### AUR

yay -S lightdm-settings apricity-icons gtk-theme-metagrip gtk-theme-numix-sx gtk-theme-shades-of-gray numix-frost-themes vertex-themes tango-icon-theme ocs-url simple-mtpfs ulauncher anydesk-bin teamviewer heroic-games-launcher flacon hypnotix vvv-bin renamemytvseries-bin shortwave tiny-media-manager-3 numix-icon-theme-pack-git pix i3lock-color ttf-ms-win10-auto ventoy-bin inxi rustdesk-bin czkawka-gui-bin siji-ttf orchis-theme-git colloid-cursors-git colloid-gtk-theme-git colloid-icon-theme-git skeuos-gtk jasper-gtk-theme-git qogir-icon-theme qogir-gtk-theme fluent-cursor-theme-git zscroll-git spotify diodon tenacity-git skeuowaita-git fluent-icon-theme-git adw-gtk3 mugshot mint-y-icons mint-x-icons mint-themes-git

##### FSTRIM

Arch linux enable ssd trim (fstrim)  

sudo systemctl enable fstrim.timer  
sudo systemctl start fstrim.timer  
sudo systemctl status fstrim.timer  

##### UFW

sudo systemctl enable ufw  
sudo ufw enable  
sudo ufw status verbose  

##### SYNCHRONIZING TIME

sudo timedatectl set-ntp true  

##### CREATE LINUX SWAP FILE (8 Gb)  (IF NEEDED)

sudo fallocate -l 8G /swapfile  
sudo chmod 600 /swapfile  
sudo mkswap /swapfile  
sudo swapon /swapfile  

Edit /etc/fstab and add  

/swapfile swap swap defaults 0 0  

To verify if swap is on  

sudo swapon –show  

or  

sudo free -h  

To remove swap file  

sudo swapoff -v /swapfile  
Remove the swap file entry /swapfile swap swap defaults 0 0 from the /etc/fstab file.  
sudo rm /swapfile  

##### Disable HDMI sound card

Call Pavucontrol  
Go to the last tab, configuration, put the hdmi source to off  
Edit the file  
/etc/pulse/default.pa and comment out load-module module-switch-on-port-available line (Around line 33)  
Save file  
Reload pulseaudio using pulseaudio -k  

##### Enable trash support in Pcmanfm

Add to you .xinitrc:  
#Start DBUS session bus:  
if [ -z "$DBUS_SESSION_BUS_ADDRESS" ]; then  
   eval $(dbus-launch --sh-syntax --exit-with-session)  
fi  

##### Set pcmanfm as default 

Go to ~/.config  
vim mimeapps.list  
insert inode/directory=pcmanfm.desktop  
save the file, no need to exit the session     

##### Set console terminal font bigger

Edit /etc/vconsole.conf  
Add the lines below:   

FONT=ter-122n  
FONT_MAP=8859-2  

##### Customize GRUB

Edit the file /etc/dafault/grub  
Remove "quiet" from GRUB_CMDLINE_LINUX_DEFAULT=  
Adjust resolution like this: GRUB_GFXMODE=1920x1080  
Add at the end:  GRUB_FORCE_HIDDEN_MENU="true"  
To call Grub using shift key at startup   
Create a file /etc/grub.d/31_hold_shift  , make executable, contents of the file at:  
https://gist.githubusercontent.com/anonymous/8eb2019db2e278ba99be/raw/257f15100fd46aeeb8e33a7629b209d0a14b9975/gistfile1.sh  
Execute the changes with sudo grub-mkconfig -o /boot/grub/grub.cfg  

##### Enable gnome-keyring at startup

Add at ~/.bash_profile  

if [ -n "$DESKTOP_SESSION" ];then  
            eval $(gnome-keyring-daemon --start)  
                 export SSH_AUTH_SOCK  
fi  

[[ -f ~/.bashrc ]] && . ~/.bashrc  

Add at ~/.xinitrc  

dbus-update-activation-environment --all  

if [ -z "$DBUS_SESSION_BUS_ADDRESS" ]; then  
    eval $(dbus-launch --sh-syntax --exit-with-session)  
fi  

Edit the file /etc/pam.d/login as below

#%PAM-1.0

auth       required     pam_securetty.so  
auth       requisite    pam_nologin.so  
auth       include      system-local-login  
auth       optional     pam_gnome_keyring.so  
account    include      system-local-login  
session    include      system-local-login  
session    optional     pam_gnome_keyring.so auto_start  

Open Seahorse and make an Default keyring with the same login password  

##### Adjusting QT5 environment

Edit /etc/environment  
Put this:  
QT_QPA_PLATFORMTHEME=qt5ct  
Reboot  

##### Blacklisting PC Speaker

Edit /etc/modprobe.d/nobeep.conf  
Put this:  
blacklist pcspkr  
Reboot  

##### Add memtest86 to Grub

Install package memtest86+-efi
Then sudo grub-mkconfig -o /boot/grub/grub.cfg
Reboot

##### Install printer for Brother DCPT-420W

Uninstall the drivers from yay  
Download the deb driver from Brother support page  
Install debtap from aur  
Set the default editor for vim: export EDITOR=vim  
At the driver download place, run the command: debtap dcpt420wpdrv-3.5.0-1.i386.deb  
When asked if need to edit the files to be created, choose option 4, the wrote vim  
Edit the architecture to i686 to x86_64 in the .PKGINFO  
Delete these lines in .INSTALL file: if [ “$(which semanage 2> /dev/null)” != ‘’ ];then; (note that you have to remove two entries at .INSTALL)  
Then execute sudo pacman -U dcpt420wpdrv-3.5.0-1-x86_64.pkg.tar.zst  

##### Install scanner for Brother DCPT-420W

yay -S brscan4  
Reboot  
